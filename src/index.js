const express = require('express');
const mongoose = require('mongoose');
const rutasCliente = require('../src/routes/Cliente');
const rutasEmpleado = require('../src/routes/Empleado')
require('dotenv').config();
const app = express();
app.use(express.json());

const port = 3005;


//middleware
app.use("/api",rutasCliente );
app.use('/api/',rutasEmpleado)






mongoose.connect(process.env.CONECCION_DB_MONGO )
.then(()=>{
    console.log("[-------------------------------------------]")
    console.log("|----------------> CONECTADO <--------------|")
    console.log("[-------------------------------------------]")

})
.catch(e =>{
    console.log(e);
})

app.get('/', (req, res) => res.send('Hello World!'))
app.listen(port, () => console.log(`Example app listening on port ${port}!`))