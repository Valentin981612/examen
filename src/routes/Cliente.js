const express = require('express');
const { execMap } = require('nodemon/lib/config/defaults');
const ClienteSchema = require("../models/Cliente");

const  router = express.Router();


router.post('/guardarCliente', async (req , res)=>{
    const Cliente = await ClienteSchema(req.body);
    if (!req.body.Nombre) {
        return res.status(401).json({
            msg: "Este campo no puede ir bacio"
        })
    }
    if (!req.body.Telefono) {
        return res.status(401).json({
            msg: "Este campo no puede ir bacio"
        })
    }
    Cliente
    .save()
    .then((data)=>{
        res.json(data);
    }).catch((error)=>{
        res.json(error)
    })
})


router.put('/EditarCliente/:id',async (req , res)=>{
    try {
        const {id} = req.params;
        const datos = req.body;
        const Cliente = await ClienteSchema.findByIdAndUpdate(id, datos);
        res.json({})
    } catch (error) {
        console.log(error)
        res.json({error})
    }
})

router.delete('/eliminarCliente/:id', async(req,res)=>{
    try {
        const {id}= req.params;
        const Cliente = await ClienteSchema.findByIdAndDelete(id);
        res.json({cliente:"Cliente Eliminado"})
    } catch (error) {
        console.log(error);
        res.json({error})
    }
})

router.get('/MostrarClientes', async(req,res)=>{
    try {
        const datos = await ClienteSchema.find().exec();
        res.json(datos)
    } catch (error) {
        
    }
})

module.exports  = router








