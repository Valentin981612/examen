const express = require('express');
const { execMap } = require('nodemon/lib/config/defaults');
const bitacoraSchema = require("../models/Bitacora");

const  router = express.Router();


router.post('/guardarBitacora', async (req , res)=>{
    const Cliente = await bitacoraSchema(req.body);
    if (!req.body.Nombre) {
        return res.status(401).json({
            msg: "Este campo no puede ir bacio"
        })
    }
    if (!req.body.EntradaSalida) {
        return res.status(401).json({
            msg: "Este campo no puede ir bacio"
        })
    }
    if (!req.body.Telefono) {
        return res.status(401).json({
            msg: "Este campo no puede ir bacio"
        })
    }
    bitacora
    .save()
    .then((data)=>{
        res.json(data);
    }).catch((error)=>{
        res.json(error)
    })
})


router.put('/Editarbitacora/:id',async (req , res)=>{
    try {
        const {id} = req.params;
        const datos = req.body;
        const bitacora = await bitacoraSchema.findByIdAndUpdate(id, datos);
        res.json({})
    } catch (error) {
        console.log(error)
        res.json({error})
    }
})

router.delete('/eliminarbitacora/:id', async(req,res)=>{
    try {
        const {id}= req.params;
        const bitacora = await bitacoraSchema.findByIdAndDelete(id);
        res.json({bitacora:"bitacora eliminada "})
    } catch (error) {
        console.log(error);
        res.json({error})
    }
})

router.get('/Mostrarbitacora', async(req,res)=>{
    try {
        const datos = await bitacoraSchema.find().exec();
        res.json(datos)
    } catch (error) {
        
    }
})

module.exports  = router
