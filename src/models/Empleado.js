const mongoose = require('mongoose')    
const Empleado = mongoose.Schema({
    Nombre : {
        type : String,
        default : 'true',
    },
    Correo : {
        type : String,
        default : 'true',
    },
    Telefono : {
        type : String,
        default : 'true',
    },
    Password : {
        type : String,
        default : 'true',
    },
    Tipo : {
        type : String,
        default : 'true',
    },
    Roles:{
        type: String,
        default: 'regular',
        enum:[
            'regular',
            'admin'
        ]

    },
    
})
module.exports =  mongoose.model( 'empleado' , Empleado)