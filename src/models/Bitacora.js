const mongoose = require('mongoose')    
const Bitacora = mongoose.Schema({
    Nombre : {
        type : String,
        required : 'true',
    },
    EntradaSalida : {
        type : String,
        required : 'true',
    },
    Telefono : {
        type : String,
        required : 'true',
    },

})
module.exports =  mongoose.model( 'bitacora' , Bitacora)