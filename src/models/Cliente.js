const mongoose = require('mongoose')    
const Cliente = mongoose.Schema({
    Nombre: {
        type : String,
        required : 'true',
    },
    Telefono: {
        type : String,
        required : 'true',
    },
})
module.exports =  mongoose.model( 'Cliente' , Cliente)