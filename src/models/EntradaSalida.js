const mongoose = require('mongoose')    
const EntradaSalida = mongoose.Schema({
    Entra : {
        type : String,
        required : 'true',
    },
    Salida : {
        type : String,
        required : 'true',
    },
})
module.exports =  mongoose.model( 'entradaSalida' , EntradaSalida)
